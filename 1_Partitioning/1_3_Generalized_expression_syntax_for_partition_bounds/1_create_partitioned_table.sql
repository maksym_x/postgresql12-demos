-- Author: Oleg Bartunov
-- URL: https://pgconf.ru/media/2019/02/20/postgresql-12-pgconfru-2019-v2.pdf

CREATE TABLE part (ts timestamp)
PARTITION BY RANGE(ts);

CREATE TABLE part1
PARTITION OF part FOR VALUES
FROM ('2018-01-01')
TO (current_timestamp + '1 day');
