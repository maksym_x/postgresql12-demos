INSERT INTO products (product_code, category_id, created)
SELECT
	(md5(random()::text)) AS product_code,
	round(random()*9999)+1 AS category_id,
	now() - '2 years'::INTERVAL * random() AS created;
