-- more about zheap: https://www.pgcon.org/2018/schedule/attachments/501_zheap-a-new-storage-format-postgresql-5.pdf

SELECT amname, amhandler, amtype FROM pg_am where amtype = 't' ORDER BY 1, 2;

CREATE ACCESS METHOD heap2 TYPE TABLE HANDLER heap_tableam_handler;

SELECT amname, amhandler, amtype FROM pg_am where amtype = 't' ORDER BY 1, 2;

CREATE TABLE t1 (ID integer, First_name text) USING heap2;

 SELECT
     pc.relkind,
     pa.amname,
     CASE WHEN relkind = 't' THEN
         (SELECT 'toast for ' || relname::regclass FROM pg_class pcm WHERE pcm.reltoastrelid = pc.oid)
     ELSE
         relname::regclass::text
     END AS relname
 FROM pg_class AS pc,
     pg_am AS pa
 WHERE pa.oid = pc.relam
    AND pa.amname = 'heap2'
 ORDER BY 3, 1, 2;
